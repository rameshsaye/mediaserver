
#include "MVExtractor.h"
#include "base/logger.h"

#include "base/datetime.h"

#include "base/filesystem.h"

#include "codec.h"

#include "Settings.h"

#include "muxer.h"

#define store "/mnt/clips-storage/"

namespace base {
namespace fmp4 {

MVExtractor::MVExtractor(std::string & cam, FrameFilter *txt, FrameFilter *muxer ): cam(cam), txt(txt), muxer(muxer)
{
    if(!txt)
      return;
        
    frame = av_frame_alloc();
    if (!frame) {
        fprintf(stderr, "Could not allocate frame\n");
        exit(1);
    }
    
    if (base::fs::exists(store + cam))
    {
        //base::fs::rmdirr(store + cam);
    }
    else
    base::fs::mkdir(store + cam);
    
    config.load(store + cam + "/"+ cam +".js");
    
}



MVExtractor::~MVExtractor() {
    if (mv_detector) {
        delete mv_detector;
        mv_detector = nullptr;
    }

     av_frame_free(&frame);
//    avcodec_free_context(&video_dec_ctx);
//    avformat_close_input(&fmt_ctx);
//    av_frame_free(&frame);

}

//int MVExtractor::read(const char *src_filename) {
//
//
//    if (avformat_open_input(&fmt_ctx, src_filename, NULL, NULL) < 0) {
//        fprintf(stderr, "Could not open source file %s\n", src_filename);
//        exit(1);
//    }
//
//    av_dump_format(fmt_ctx, 0, src_filename, 0);
//
//    return 0;
//}



int MVExtractor::set() 
{
    
        ///AVStream *st;
        
        AVCodec *dec = NULL;
        int ret;
        AVDictionary *opts = NULL;
           
//        st = fmt_ctx->streams[0];
//
//       
//
//       int ret = avcodec_parameters_to_context(video_dec_ctx, st->codecpar);
//        if (ret < 0) {
//            fprintf(stderr, "Failed to copy codec parameters to codec context\n");
//            return ret;
//        }

        /* Init the video decoder */
        av_dict_set(&opts, "flags2", "+export_mvs", 0);
        if ((ret = avcodec_open2(video_dec_ctx, dec, &opts)) < 0) {
            fprintf(stderr, "Failed to open codec\n");
            return ret;
        }
        

     mv_detector = new MVDetector(std::make_pair(dst_width, dst_height));
  
}

//int MVExtractor::decode_packet(int *got_frame, int cached) {
//    int decoded = pkt.size;
//
//    *got_frame = 0;
//
//    if (pkt.stream_index == video_stream_idx) {
//        int ret = avcodec_decode_video2(video_dec_ctx, frame, got_frame, &pkt);
//        if (ret < 0) {
//            fprintf(stderr, "Error decoding video frame \n");
//            return ret;
//        }
//
//        if (*got_frame) {
//            int i;
//            AVFrameSideData *sd;
//
//            char pict_type = av_get_picture_type_char(frame->pict_type);
//
//            // get pts
//            int64_t pts = frame->pts != AV_NOPTS_VALUE ? frame->pts : (frame->pkt_dts != AV_NOPTS_VALUE ? frame->pkt_dts : pts + 1);
//
//
//            video_frame_count++;
//            sd = av_frame_get_side_data(frame, AV_FRAME_DATA_MOTION_VECTORS);
//            //            if (sd) 
//            //            {
//            //                const AVMotionVector *mvs = (const AVMotionVector *)sd->data;
//            //                for (i = 0; i < sd->size / sizeof(*mvs); i++) {
//            //                    const AVMotionVector *mv = &mvs[i];
//            //                    printf("%d,%2d,%2d,%2d,%4d,%4d,%4d,%4d,0x%"PRIx64"\n",
//            //                           video_frame_count, mv->source,
//            //                           mv->w, mv->h, mv->src_x, mv->src_y,
//            //                           mv->dst_x, mv->dst_y, mv->flags);
//            //                    
//            //                    
//            //                }
//            //            }
//
//
//            if (sd != nullptr) { // sd == nullptr when I frame also
//                // reading motion vectors, see ff_print_debug_info2 in ffmpeg's libavcodec/mpegvideo.c for reference and a fresh doc/examples/extract_mvs.c
//                AVMotionVector* mvs = (AVMotionVector*) sd->data;
//                int mvcount = sd->size / sizeof (AVMotionVector);
//                //Measure m2("proc");
//                bool movement = mv_detector->process_frame(pts, video_frame_count, pict_type, std::vector<AVMotionVector>(mvs, mvs + mvcount));
//                //std::cout << "proc: " << m2.elapsed() << std::endl;
//
//                //                mv_detector->draw_occupancy(img2);
//                //                mv_detector->draw_motion_vectors(img3);
//                //std::cout << "avg_movment=" << avg_movement << std::endl;
//
//                if (movement) {
//                    //  cv::putText(img2, "Movement", cv::Point(10, 200), cv::FONT_HERSHEY_SIMPLEX, 2, CV_RGB(0, 0, 255), 2, cv::LINE_AA);
//
//                    std::cout << "trigger Motion detection events for frame " << video_frame_count << " frame type " << pict_type << std::endl << std::flush;
//                } else {
//                    std::cout << " frame " << video_frame_count << " frame type " << pict_type << std::endl << std::flush;
//                }
//                //                cv::imshow("motion vectors", img3);
//                //                cv::imshow("occupancy", img2);
//                //#if 1
//                //                switch (cv::waitKey(run_mode == 'r' ? 10 : 0)) {
//                //                    case 0x1b:
//                //                        break;
//                //                    case 'p':
//                //                        run_mode = cv::waitKey(0) != 'r' ? 'p' : 'r';
//                //                        break;
//                //                    default:
//                //                        run_mode = run_mode != 'r' ? 'p' : 'r';
//                //                }
//                //#endif
//            } else {
//                mv_detector->process_frame(pts, video_frame_count, pict_type, std::vector<AVMotionVector>());
//            }
//
//
//        }
//    }
//
//    return decoded;
//}

//int MVExtractor::open_codec_context(AVFormatContext *fmt_ctx, enum AVMediaType type) {
//    int ret;
//    AVStream *st;
//    AVCodecContext *dec_ctx = NULL;
//    AVCodec *dec = NULL;
//    AVDictionary *opts = NULL;
//
//    ret = av_find_best_stream(fmt_ctx, type, -1, -1, &dec, 0);
//    if (ret < 0) {
//        fprintf(stderr, "Could not find %s stream in input file\n",
//                av_get_media_type_string(type));
//        return ret;
//    } else {
//        int stream_idx = ret;
//        st = fmt_ctx->streams[stream_idx];
//
//        dec_ctx = avcodec_alloc_context3(dec);
//        if (!dec_ctx) {
//            fprintf(stderr, "Failed to allocate codec\n");
//            return AVERROR(EINVAL);
//        }
//
//        ret = avcodec_parameters_to_context(dec_ctx, st->codecpar);
//        if (ret < 0) {
//            fprintf(stderr, "Failed to copy codec parameters to codec context\n");
//            return ret;
//        }
//
//        /* Init the video decoder */
//        av_dict_set(&opts, "flags2", "+export_mvs", 0);
//        if ((ret = avcodec_open2(dec_ctx, dec, &opts)) < 0) {
//            fprintf(stderr, "Failed to open %s codec\n",
//                    av_get_media_type_string(type));
//            return ret;
//        }
//
//        video_stream_idx = stream_idx;
//        video_stream = fmt_ctx->streams[video_stream_idx];
//        video_dec_ctx = dec_ctx;
//
//        dst_width = video_stream->codecpar->width;
//        dst_height = video_stream->codecpar->height;
//
//
//        mv_detector = new MVDetector(std::make_pair(dst_width, dst_height));
//
//    }
//
//    return 0;
//}

//void MVExtractor::extract(){
//
//    
//    int ret = 0, got_frame;
//    
//    
//    while (av_read_frame(fmt_ctx, &pkt) >= 0) {
//        AVPacket orig_pkt = pkt;
//        do {
//            ret = decode_packet(&got_frame, 0);
//            if (ret < 0)
//                break;
//            pkt.data += ret;
//            pkt.size -= ret;
//        } while (pkt.size > 0);
//        av_packet_unref(&orig_pkt);
//    }
//
//    /* flush cached frames */
//    pkt.data = NULL;
//    pkt.size = 0;
//    do {
//        decode_packet(&got_frame, 1);
//    } while (got_frame);
//}

int MVExtractor::send()
{
    
    
    if( continueMotion ==  Settings::configuration.MOTIONCOUNT &&  !iFrameCount)
    {
        ++iFrameCount ;
      
        
       // std::time_t ct = std::time(0);
       // std::string time = std::ctime(&ct);
        
         std::string res;
         Timestamp ts;
         DateTimeFormatter::append(res, ts, DateTimeFormat::SORTABLE_FORMAT);
                    
        
        txtFrame.txt= "MD_" + cam + "_" + res;
        
        config.setRaw(txtFrame.txt, "") ;
        
        txt->run( &txtFrame);
        SInfo <<  "Send MD Trigger";
        
        
    }
    
}

int MVExtractor::extract( AVPacket *pk ,  short unsigned slice_type){
    
    FragMP4MuxFrameFilter *mx   =  (FragMP4MuxFrameFilter*)muxer;

    int decoded = pk->size;

    int got_frame = 0;
    
    if(slice_type  ==   H264SliceType::none )
    {
      //  base::fs::savefile( cam + "/spspps" , (const char*) pk->data, pk->size );
       spspps = std::string((const char*) pk->data, pk->size   );
    }
   
/*  wrong values please check it, before enabling it
    else if(slice_type  ==   H264SliceType::idr )
    {
      
    }
    else if(slice_type  ==   H264SliceType::nonidr )
    {
        // base::fs::savefile( cam + "/spspps" , (const char*) pk->data, pk->size );
    }
  */  
        

    if (pk->stream_index == video_stream_idx) {
        int ret = avcodec_decode_video2(video_dec_ctx, frame, &got_frame, pk);
        if (ret < 0) {
            fprintf(stderr, "Error decoding video frame \n");
            return ret;
        }

        if (got_frame) {
            int i;
            AVFrameSideData *sd;

            char pict_type = av_get_picture_type_char(frame->pict_type);
            
            
            if( AVPictureType::AV_PICTURE_TYPE_I == frame->pict_type && iFrameCount  )
            {
                
                if( ++iFrameCount >  Settings::configuration.CLIPSIZE  )
                {
                    iFrameCount = 0;
                    
                    /////////////////
                    std::string res;
                    Timestamp ts;
                    DateTimeFormatter::append(res, ts, DateTimeFormat::SORTABLE_FORMAT);
                    
                    base::fs::savefile( store + cam + "/" +res +".264", (const char*) h264.data(),h264.size() );
                    
                    config.setRaw(txtFrame.txt,  store + cam + "/" +res +".264") ;
                    config.save();
                    
                    
                    if( mx->got_ftyp && mx->got_moov)
                    {
                         mx->clipData  = std::string((const char*) mx->ftyp_frame.payload.data(), mx->ftyp_frame.payload.size()) + std::string((const char*) mx->moov_frame.payload.data(), mx->moov_frame.payload.size()) + mx->clipData;
                         
                         base::fs::savefile(  store + cam + "/" +res +".mp4", (const char*) mx->clipData.data(),mx->clipData.size() );
                    }
                    
                    
                    mx->clipData.clear();
                    
                    h264.clear();
                    if(spspps.size())
                        h264 += spspps ;
                    
                }
            }
            else if(AVPictureType::AV_PICTURE_TYPE_I == frame->pict_type && !iFrameCount)
            {
               h264.clear();
               mx->clipData.clear();
               if(spspps.size())
               h264 += spspps ;

            }
           
//            else if(slice_type  ==   H264SliceType::nonidr )
//            {
//                // base::fs::savefile( cam + "/spspps" , (const char*) pk->data, pk->size );
//            }
            if(h264.size())
            {
             
               h264 = h264 +  std::string( (const char*)  pk->data, pk->size);
               
              // mx->clipData  += std::string((const char*) mx->internal_frame.payload.data(), mx->internal_frame.payload.size());
             
            }
            
            
          //  printf(" Pic type  %c  %d\n " , pict_type, decoded );
            // get pts
            int64_t pts = frame->pts != AV_NOPTS_VALUE ? frame->pts : (frame->pkt_dts != AV_NOPTS_VALUE ? frame->pkt_dts : pts + 1);


            video_frame_count++;
            sd = av_frame_get_side_data(frame, AV_FRAME_DATA_MOTION_VECTORS);
            //            if (sd) 
            //            {
            //                const AVMotionVector *mvs = (const AVMotionVector *)sd->data;
            //                for (i = 0; i < sd->size / sizeof(*mvs); i++) {
            //                    const AVMotionVector *mv = &mvs[i];
            //                    printf("%d,%2d,%2d,%2d,%4d,%4d,%4d,%4d,0x%"PRIx64"\n",
            //                           video_frame_count, mv->source,
            //                           mv->w, mv->h, mv->src_x, mv->src_y,
            //                           mv->dst_x, mv->dst_y, mv->flags);
            //                    
            //                    
            //                }
            //            }


            if (sd != nullptr) { // sd == nullptr when I frame also
                // reading motion vectors, see ff_print_debug_info2 in ffmpeg's libavcodec/mpegvideo.c for reference and a fresh doc/examples/extract_mvs.c
                AVMotionVector* mvs = (AVMotionVector*) sd->data;
                int mvcount = sd->size / sizeof (AVMotionVector);
                //Measure m2("proc");
                bool movement = mv_detector->process_frame(pts, video_frame_count, pict_type, std::vector<AVMotionVector>(mvs, mvs + mvcount));
                //std::cout << "proc: " << m2.elapsed() << std::endl;

                //                mv_detector->draw_occupancy(img2);
                //                mv_detector->draw_motion_vectors(img3);
                //std::cout << "avg_movment=" << avg_movement << std::endl;

                if (movement && !iFrameCount )
                {
                    
                    //  cv::putText(img2, "Movement", cv::Point(10, 200), cv::FONT_HERSHEY_SIMPLEX, 2, CV_RGB(0, 0, 255), 2, cv::LINE_AA);
                    if (continueMotion < Settings::configuration.MOTIONCOUNT)
                    {
                        continueMotion++;
                        
                       // if( continueMotion == MOTIONCOUNT)
                      //  iFrameCount++; 
                        
                        base::SInfo << "trigger Motion detection events for frame " << video_frame_count << " frame type " << pict_type ;
                    }
                } 
                else 
                {
                    continueMotion = 0;
                   // std::cout << " frame " << video_frame_count << " frame type " << pict_type << std::endl << std::flush;
                }
                //                cv::imshow("motion vectors", img3);
                //                cv::imshow("occupancy", img2);
                //#if 1
                //                switch (cv::waitKey(run_mode == 'r' ? 10 : 0)) {
                //                    case 0x1b:
                //                        break;
                //                    case 'p':
                //                        run_mode = cv::waitKey(0) != 'r' ? 'p' : 'r';
                //                        break;
                //                    default:
                //                        run_mode = run_mode != 'r' ? 'p' : 'r';
                //                }
                //#endif
            } else {
                mv_detector->process_frame(pts, video_frame_count, pict_type, std::vector<AVMotionVector>());
            }


        }
    }
    
    
    
    send();

    

    return decoded;
}


}
}
