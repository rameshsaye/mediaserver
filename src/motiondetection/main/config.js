{
  "dtlsCertificateFile": "/var/tmp/key/certificate.crt",
  "dtlsPrivateKeyFile": "/var/tmp/key/private_key.pem",
  "listenIps": [
    {
      "announcedIp": "52.5.6.245",
      "ip": "0.0.0.0"
    }
  ],
  "logLevel": "info",
  "logTags": [
    "info"
  ],
  "potentialID": "729795",
  "rtcMaxPort": 12560,
  "rtcMinPort": 11501,
  "CLIPSIZE": 5,
  "MOTIONCOUNT": 1,

  "DEFAULT_WINDOW_SIZE": 3,
  "DEFAULT_OCCUPANCY_THRESHOLD": 2,
  "DEFAULT_LOCAL_OCCUPANCY_AVG_THRESHOLD":  0.6,
  "DEFAULT_OCCUPANCY_AVG_THRESHOLD": 0.6,

  "rtsp": {
    "0": {
      "rtsp": "rtsp://localhost/test.264",
      "state": "streaming",
      "md": true
    },
    "1": {
      "rtsp": "rtsp://localhost/test1.264",
      "state": "streaming",
      "md": true
    },
    "2": {
      "rtsp": "rtsp://localhost/test2.264",
      "state": "streaming",
      "md": true
    },
    "3": {
      "rtsp": "rtsp://localhost/test3.264",
      "state": "streaming"
    },
    "4": {
      "rtsp": "rtsp://localhost/test4.264"
    },
    "229795F1234CAM14567": {
      "rtsp": "rtsp://root:60056006@10.86.8.16:559/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25"
    },
    "429795F1234CAM14561": {
      "rtsp": "rtsp://root:0011J00001MUvfQQAT@10.86.0.24:554/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25"
    },
    "529795F1234CAM14562": {
      "rtsp": "rtsp://root:001o000000vmbHtAAI@10.86.12.100:558/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25"
    },
    "629795F1234CAM14569": {
      "rtsp": "rtsp://root:001o000000vmbHtAAI@10.86.12.100:554/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25"
    },
    "729795F1234CAM14528": {
      "rtsp": "rtsp://root:60056005@10.76.0.16:555/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25"
    },
    "929795F1234CAM14569": {
      "rtsp": "rtsp://root:60056006@10.86.8.16:560/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25",
      "md": true
    },
    "Cam04": {
      "rtsp": "rtsp://root:60056006@10.86.8.16:555/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25",
      "md": true
    },
    "Cam09": {
      "rtsp": "rtsp://root:60056006@10.86.8.16:558/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25",
      "md": true
    },
    "Cam10": {
      "rtsp": "rtsp://root:60056006@10.86.8.16:572/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25",
      "md": true
    },
    "Cam11": {
      "rtsp": "rtsp://root:60056006@10.86.8.16:574/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25",
      "md": true
    },
    "Cam12": {
      "rtsp": "rtsp://root:60056005@10.86.8.16:561/axis-media/media.amp?videocodec=h264&resolution=768x576&fps=25",
      "md": true
    }
  },
  "stream_type": "webrtc"
}