extern "C" {
#include <libavutil/motion_vector.h>
#include <libavformat/avformat.h>
}

#include "MVDetector.h"
#include "framefilter.h"


#include "json/configuration.h"


#ifndef H264_MVE_H
#define H264_MVE_H


//#define MOTIONCOUNT 2

//#define CLIPSIZE  5

namespace base {
namespace fmp4 {

class MVExtractor {
public:

    MVExtractor(std::string & cam,  FrameFilter *txt, FrameFilter *muxer) ;
   // MVExtractor(const char *src_filename);

    ~MVExtractor();
    
    int set();
    

    AVFormatContext *fmt_ctx{NULL};
    AVCodecContext *video_dec_ctx{NULL};
    AVStream *video_stream{NULL};
    //const char *src_filename{NULL};

    int video_stream_idx{0};
    AVFrame *frame{NULL};
    AVPacket pkt;
    int video_frame_count{0};

    int dst_width{0};
    int dst_height{0};
    //void extract();
    int extract( AVPacket *pk ,   short unsigned slice_type);
    
    int continueMotion{0};
       
    int iFrameCount{0};
    
    std::string  cam;
    
    std::string spspps;
    std::string h264;
    
    TextFrame txtFrame;
      
    int send();
private:

    MVDetector *mv_detector{nullptr};
    
    FrameFilter *txt;
    
    FrameFilter *muxer;
    
    base::cnfg::Configuration config;

    //int open_codec_context(AVFormatContext *fmt_ctx, enum AVMediaType type);

   // int decode_packet(int *got_frame, int cached);

   // int read(const char *src_filename);



};

}
}
#endif
